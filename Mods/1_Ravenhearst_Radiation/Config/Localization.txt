﻿Key,Source,Context,English
toolGeigerRH,items,Tool,Geiger Counter,,,,,
toolGeigerRHDesc,items,Tool,"This geiger counter needs to be in your hand or on your toolbelt. Listen carefully. When you hear the meter going off this means you are near a radiated area. This can kill you without a full Hazmat suit.",,,,,
