﻿Key,Source,Context,English
meleeWpnClubT3AluminumBat,items,Melee,Steel Bat,,,,,
meleeWpnClubT3AluminumBatDesc,items,Melee,,,"Steel bats are very durable and hard-hitting.\n\nRepair with a Repair Kit.\nScrap to Iron.",,,,,
meleeWpnClubT3AluminumBatSchematic,items,Melee,Steel Bat Schematic,,,,,
